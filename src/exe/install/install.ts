/*
 *  Add ghata itself as a dependency because npx deletes the adapter
 *  once we finish this process, to make things persistent we will install
 *  ghata as a dependency to Ghost.
 *  Created On 08 May 2020
 */

import exec from 'execa'

import logger from '../logger'

export default async function installGhata(
    ghostPath: string,
    force: boolean,
    ghostInfo: any,
): Promise<void> {
    // read my version from my own package.json
    const version = require('../../../package.json').version

    if (!ghostInfo.dependencies['ghata'] || force == true) {
        logger.verbose('Installing ghata as a dependency to Ghost')

        await exec('yarn', ['add', `ghata@${version}`], {
            cwd: ghostPath,
        })
    }
}
